require("lucy.config")
-- no db here, fuckers
-- require("lucy.db")

-- configure db connections by creating /lua/lucy/db.lua with the following
-- content: 

-- vim.cmd [[
-- let g:dbs = [
-- \ { 'name': 'Example OAuth2 Authenticated', 'url': 'sqlserver://username@server/database?authentication=ActiveDirectoryInteractive'},
-- \ ]
-- ]]



local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup("plugins")
