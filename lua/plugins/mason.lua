return {
    {
        "williamboman/mason.nvim",
        config = function()
            require('mason').setup()
        end,
    },
    {
        "williamboman/mason-lspconfig.nvim",
        opts = {
            ensure_installed = {"lua_ls", "sqlls", "rust_analyzer", "pyright", "clangd"},
            automatic_installation = true,
        }
    },
    {
        "neovim/nvim-lspconfig",
        config = function()
            local lspconfig = require('lspconfig')
            lspconfig.rust_analyzer.setup {}
            lspconfig.lua_ls.setup {}
            lspconfig.sqlls.setup {}
            lspconfig.pyright.setup {}
	    lspconfig.clangd.setup {}
        end,
    },
}
